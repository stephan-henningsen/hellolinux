# Buildroot + Docker + Makefile + QEMU = <3

Build a minimalistic Linux system in a bootable ISO file using Buildroot within Docker, without getting your hands or machine dirty from toolchains, build files, etc.  Optionally boot it from QEMU or a USB stick.



##  Quick start


```
time docker build \
	--build-arg BUILD_UID=$(id -u) \
    --build-arg BUILD_GID=$(id -g) \
	-t hellolinux \
	context

docker run --rm -it -v "$(pwd)":/host --name hellolinux-factory hellolinux

qemu-system-x86_64 -M pc \
	-drive file=hellolinux.iso,if=virtio,format=raw \
	-net nic,model=virtio -net user	
```

Or if you're so lucky that you have Make on your system:

```
make iso     # This will take about half an hour.
make qemu
```



##  Details


`make iso` will basically run `dbuild` and `drun`.

`make dbuild` will run `docker build` and download, configure, and compile buildroot complete with Linux kernel, busybox, uClibc, etc.  The UID and GID of your user are sent into docker so that the files created in the docker images are owned by you.

`make drun` will run `docker run` and create bind-mount the current directory into the container, and the container will copy bzImage, rootfs, and ISO file here.

`make qemu` will start QEMU and boot the ISO file created previously.

`make debug` will build a Docker container to a certain stage and enter it.



##  Other interesting uses

```
make qemu-cpio
make debug
make clean # Remove output files and running containers.
make cleanclean # Remove cached docker image too.  A full Buildroot rebuld is required hereafter.
```

Press Ctrl + Alt +F to toggle full-screen


