.PHONY: Dockerfile context build run images iso qemu-ext2 qemu-cpio qemu-iso clean cleanclean all boot debug

dbuild: Dockerfile context
	time docker build \
		--build-arg BUILD_UID=$$(id -u) \
		--build-arg BUILD_GID=$$(id -g) \
		-t hellolinux \
		context

drun:	dbuild
	docker run --rm -it -v "$$(pwd)":/host --name hellolinux-factory hellolinux



bzImage rootfs.ext2 rootfs.cpio hellolinux.iso: drun

images:	bzImage rootfs.ext2 rootfs.cpio hellolinux.iso

iso: hellolinux.iso



qemu-ext2: bzImage rootfs.ext2
	qemu-system-x86_64 -M pc \
	-kernel bzImage \
	-drive file=rootfs.ext2,if=virtio,format=raw \
	-append "root=/dev/vda" \
	-net nic,model=virtio -net user

qemu-cpio: bzImgage rootfs 
	qemu-system-x86_64 -M pc \
	-kernel bzImage \
	-initrd rootfs.cpio \
	-net nic,model=virtio -net user

qemu-iso: hellolinux.iso
	qemu-system-x86_64 -M pc \
	-drive file=hellolinux.iso,if=virtio,format=raw \
	-net nic,model=virtio -net user	



qemu:	qemu-iso

debug:
	docker build \
		--build-arg BUILD_UID=$$(id -u) \
		--build-arg BUILD_GID=$$(id -g) \
		-t hellolinux-debug \
		--target=build \
		context && \
	docker run --rm -it -v "$$(pwd)":/host --name hellolinux-debug hellolinux-debug



clean:
	docker rm -f hellolinux-factory hellolinux-debug || true # Don't fail if images don't exist.
	rm -f bzImage rootfs.ext2 rootfs.cpio initrd.img hellolinux.iso 

cleanclean: clean
	docker rmi -f hellolinux hellolinux-debug

all:	images



